.. Read the Docs Template documentation master file, created by
   sphinx-quickstart on Tue Aug 26 14:19:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

QCT CORD POD 5.0 Installation Guide
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This document is the installation guilde of CORD POD 5.0.

current version: 3.0


.. toctree::
  :maxdepth: 2
  :caption: Table of Contents

  authors

  hardware_req

  prerequisite

  prepare_cicd_env

  deploy_cord

  functional_test

  demo/graphviz


References
^^^^^^^^^^

- `vscode reStructuredText Live Preview <https://github.com/vscode-restructuredtext/vscode-restructuredtext/blob/master/docs/sphinx.md>`_

+ `reStructuredText(rst)快速入門語法說明 - IT閱讀 <http://www.itread01.com/articles/1475823849.html>`_

