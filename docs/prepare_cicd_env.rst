Prepare CICD Environment
^^^^^^^^^^^^^^^^^^^^^^^^

version 3.0

Install OpenShift
-----------------

This is the guide of installing `OpenShift Origin <https://www.openshift.org/>`_

Prepare Virtual Machine
>>>>>>>>>>>>>>>>>>>>>>>

Needs a Ubuntu 16.04 virtual machine.


.. disqus::
