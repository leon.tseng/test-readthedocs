graphviz Demo
^^^^^^^^^^^^^

There are few graphviz example here


Diagram 1
---------

.. graphviz::

  graph {
    a -- b[color=red,penwidth=3.0];
    b -- c;
    c -- d[color=red,penwidth=3.0];
    d -- e;
    e -- f;
    a -- d;
    b -- d[color=red,penwidth=3.0];
    c -- f[color=red,penwidth=3.0];
  }

Diagram 2
---------

.. graphviz::

  digraph example {
      a [label="sphinx", href="http://sphinx-doc.org", target="_top"];
      b [label="other"];
      a -> b;
  }

Diagram 3
---------

.. graphviz::

  digraph {
    subgraph cluster_0 {
        label="Subgraph A";
        a -> b;
        b -> c;
        c -> d;
    }

    subgraph cluster_1 {
        label="Subgraph B";
        a -> f;
        f -> c;
    }
  }

Diagram 4
---------

.. graphviz::

  graph {
    rankdir=LR; // Left to Right, instead of Top to Bottom
    a -- { b c d };
    b -- { c e };
    c -- { e f };
    d -- { f g };
    e -- h;
    f -- { h i j g };
    g -- k;
    h -- { o l };
    i -- { l m j };
    j -- { m n k };
    k -- { n r };
    l -- { o m };
    m -- { o p n };
    n -- { q r };
    o -- { s p };
    p -- { s t q };
    q -- { t r };
    r -- t;
    s -- z;
    t -- z;
  }


Diagram 5
---------

.. graphviz::

  graph {
    rankdir=LR;
    a -- { b c d }; b -- { c e }; c -- { e f }; d -- { f g }; e -- h;
    f -- { h i j g }; g -- k; h -- { o l }; i -- { l m j }; j -- { m n k };
    k -- { n r }; l -- { o m }; m -- { o p n }; n -- { q r };
    o -- { s p }; p -- { s t q }; q -- { t r }; r -- t; s -- z; t -- z;
    { rank=same; b, c, d }
    { rank=same; e, f, g }
    { rank=same; h, i, j, k }
    { rank=same; l, m, n }
    { rank=same; o, p, q, r }
    { rank=same; s, t }
  }


Diagram 6
---------

.. graphviz::

  graph G {
    "黑海" [shape = circle, color = blueviolet, fontcolor = blueviolet, fontsize = 20];
    "黑海" -- "亞速海" [label = "刻赤海峽"];

    subgraph cluster_T {
      label = "黑海海峽";
      fontsize = 24;
      fillcolor = darkslategray;
      style = filled;
      fontcolor = white;
      node [fontcolor = white, color = white];
      "博斯普魯斯海峽" -- "馬爾馬拉海" -- "達達尼爾海峽" [color = white];
      "博斯普魯斯海峽" [shape = parallelogram];
      "達達尼爾海峽" [shape = parallelogram];
    }

    "黑海" -- "博斯普魯斯海峽" [color = red ,penwidth = 2];
    "達達尼爾海峽" -- "愛琴海" [color = red ,penwidth = 2];

    subgraph cluster_M {
      label = "地中海海域";
      fontsize = 24;
      "西部地中海" [shape = Mcircle, style = filled, color = grey, fillcolor = aquamarine, fontsize = 20];
      "中部地中海" [shape = Mcircle, style = filled, color = grey, fillcolor = aquamarine, fontsize = 20];
      "直布羅陀海峽" [shape = parallelogram, fontcolor = red];
      "西西里海峽" [shape = parallelogram];
      "中部地中海" -- {"愛琴海" "愛奧尼亞海" "西西里海峽"};
      "西部地中海" -- {"西西里海峽" "第勒尼安海" "利古里亞海" "伊比利海" "阿爾沃蘭海"};
      "愛奧尼亞海" -- "亞得裡亞海";
      "阿爾沃蘭海" -- "直布羅陀海峽";
    }
  }